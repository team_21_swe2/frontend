import React from "react";
import TableauReport from 'tableau-react';
const options = {
  width: window.innerHeight / 2,
  height: window.innerHeight / 2,
  hideTabs: false,
  // All other vizCreate options are supported here, too
  // They are listed here: https://onlinehelp.tableau.com/current/api/js_api/en-us/JavaScriptAPI/js_api_ref.htm#ref_head_9
};
// TODO: Automated sheets checking on Tableau per naming conventions 
//      -> If name exsist, populate on webapp
const Viz = prop => {

  if (prop.vizReq === "daily") {
    return (
      <div>
        <TableauReport
          url="https://public.tableau.com/views/DailyDiagnostics/Sheet1"
        />
      </div>
    )
  }
  if (prop.vizReq === "on-demand") {
    return (
      <div>
        <TableauReport
          url="https://public.tableau.com/views/On-DemandDiagnostics/Sheet1"
        // options={options}
        />
      </div>
    )
  }
  if (prop.vizReq === "map") {

    return (
      <div>
        <TableauReport
          url="https://public.tableau.com/views/On-DemandDiagnostics/Sheet3"
        // options={options}
        />
      </div>
    )
  }
}

export default Viz;
