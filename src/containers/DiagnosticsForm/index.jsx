import React from "react";

const mockDiagnosticData = [{
  gwID: 123456,
  wfID: 12345678,
  timestamp: Date.now(),
  gearboxTemp: 62,
  rotorTemp: 45,
  rotorSpeed: 150,
  gridVoltage: 2100
},
{
  gwID: 654321,
  wfID: 87654321,
  timestamp: Date.now() - 1,
  gearboxTemp: 65,
  rotorTemp: 64,
  rotorSpeed: 141,
  gridVoltage: 2091
}]

console.log("diagnostics")
// Mock data until endpoint works
function Diagnostics() {
  return (
    <div>
      {mockDiagnosticData.map((data) =>
        <li>
          <b>Gateway ID:</b> {data.gwID} <br />
          <b>Wind Farm ID:</b>{data.wfID} <br />
          <b>Timestamp:</b>{data.timestamp} <br />
          <b>Gearbox Tempature:</b>{data.gearboxTemp} <br />
          <b>Rotor Tempature:</b>{data.rotorTemp} <br />
          <b>Rotor Speed:</b>{data.rotorSpeed} <br />
          <b>Grid Voltage:</b>{data.gridVoltage} <br />
          <br />
        </li>)}
    </div>
  )
}

export default Diagnostics;