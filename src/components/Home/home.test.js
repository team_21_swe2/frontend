import React from "react";
import { render, cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";
import Home from "../Home";

afterEach(cleanup);

it("Fetches heartbeat data", async () => {
  const { getByTestId } = render(<Home />);
  expect(getByTestId("greeting")).toHaveTextContent("Hello Wind Farm Manager!");
   
});