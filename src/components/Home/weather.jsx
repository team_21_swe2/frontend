import React, { Component } from 'react';
import axios from "axios"
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";


class Weather extends Component {
  constructor(props) {
    super(props);

    this.state = {
      main: "",
      description: "",
      speed: "",
      deg: "",
      tomorrow: "",
      tmain: "",
      tdescription: "",
      tspeed: "",
      tdeg: ""
    };
  }

  async componentDidMount() {
    const API_KEY = "635317b88921df31c98adf5d42c06d8d";
    const resWeather = await fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=Austin,us&APPID=${API_KEY}`
    )
    const jsonWeather = await resWeather.json();
    this.setState({
      main: jsonWeather.weather[0].main,
      description: jsonWeather.weather[0].description,
      speed: jsonWeather.wind.speed,
      deg: jsonWeather.wind.deg
    });

    const resForecast = await fetch(
      `https://api.openweathermap.org/data/2.5/forecast?q=Austin,us&APPID=${API_KEY}`
    )
    const jsonForecast = await resForecast.json();
    const dateString = jsonForecast.list[0].dt_txt;
    const formateDate = dateString.substring(0, 10);
    this.setState({
      date: formateDate,
      tmain: jsonForecast.list[0].weather[0].main,
      tdescription: jsonForecast.list[0].weather[0].description,
      tspeed: jsonForecast.list[0].wind.speed,
      tdeg: jsonForecast.list[0].wind.deg
    });
    console.log(this.state)
  };

  render() {
    const { paper } = this.props;
    const { main, description, speed, deg, date, tmain, tdescription, tspeed, tdeg } = this.state
    return (
      <div>
        <Paper className={paper}>
          <Typography variant="h4" component="h3">
            Current Weather:
              </Typography>
          <Typography variant="h5" component="h3">
            Austin, TX
              </Typography>
          <Typography component="p"></Typography>
          <Typography component="p">
            Main: <b>{main} </b><br />
            Description: <b> {description} </b><br />
            Wind Speed: <b> {speed} meter/sec </b> <br />
            Wind Degree: <b>{deg}°</b>
          </Typography>
        </Paper>
        <Paper className={paper}>
          <Typography variant="h4" component="h3">
            Forecast Weather:
              </Typography>
          <Typography variant="h5" component="h3">
            Austin, TX
              </Typography>
          <Typography component="p">
            Date: <b> {date} </b> <br />
            Main: <b>{tmain} </b><br />
            Description: <b> {tdescription} </b><br />
            Wind Speed: <b> {tspeed} meter/sec </b> <br />
            Wind Degree: <b>{tdeg}°</b>
          </Typography>
        </Paper>
      </div>

    );
  }
}

export default Weather;