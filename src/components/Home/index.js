import React from "react";
import Viz from "../../containers/Tableau";
import "./index.css";
import { makeStyles, Container, Grid } from "@material-ui/core";
import Weather from "./weather";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  container: {
    marginLeft: 57,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    // textAlign: "center",
    color: theme.palette.text.primary,
    margin: theme.spacing(2)
  }
}));

function Headline() {
  const classes = useStyles();

  return (
    <div className="home-wapper">
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>
          <Grid item xs={8}>
            <Viz vizReq="map" />
          </Grid>
          <Grid item xs={4}>
            <Weather paper={classes.paper} />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default Headline;
