import React, { Component } from "react";
import axios from "axios";
import MaterialTable from "material-table";
import RefreshIcon from "@material-ui/icons/Refresh";
import { Button } from "@material-ui/core";
import { setTimeout } from "timers";
import "./index.css";

export default class HeartbeatFeed extends Component {
  constructor(props) {
    super(props);

    this.state = {
      heartbeatFeed: []
    };

    this.heartbeatFeed = this.heartbeatFeed.bind(this);
  }

  heartbeatFeed = () => {
    console.log("loop");
    axios
      .get(
        `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/heartbeats`
      )
      .then(res => {
        const heartbeats = res.data;

        // console.log(diagnostics);
        this.setState({ heartbeatFeed: heartbeats });
      });

    // setTimeout(this.heartbeatFeed, 10000);
  };

  componentDidMount() {
    this.heartbeatFeed();
  }

  render() {
    const { heartbeatFeed } = this.state;
    return (
      <div style={{ maxWidth: "100%" }}>
        <MaterialTable
          options={{ pageSize: 5 }}
          columns={[
            { title: "GW ID", field: "gwID", type: "numeric" },
            {
              title: "Sent Time",
              field: "sentTime",
              type: "date"
            },
            {
              title: "Received Date",
              field: "receivedTime.date",
              type: "date"
            },
            {
              title: "Received Time",
              field: "receivedTime.time",
              type: "string"
            }
          ]}
          data={heartbeatFeed}
          title="Heartbeat Feed"
        />
        <div className="center">
          {" "}
          <br />
          <br />
          <br />
          <Button
            variant="contained"
            color="primary"
            // className="center"
            startIcon={<RefreshIcon />}
            onClick={this.heartbeatFeed}
          >
            Refresh
          </Button>
        </div>
      </div>
    );
  }
}
