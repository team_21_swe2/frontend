import React from "react";
import Card from "./card";
import { makeStyles, Container, Grid } from "@material-ui/core";
import { Redirect, Link, Route, withRouter } from "react-router-dom";
import HeartbeatFeed from "./heartbeatFeed";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    marginLeft: 57,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  }
}));

const Heartbeat = props => {
  const classes = useStyles();
  const [gateways, setGateways] = React.useState([]);
  React.useEffect(() => {
    fetchGateways();
  }, []);

  const fetchGateways = async () => {
    let fetchGatewaysResp = await fetch(
      `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/gateways/`,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    if (fetchGatewaysResp.ok) {
      setGateways(await fetchGatewaysResp.json());
    }
    console.log(JSON.stringify(gateways));
  };

  const handleRedirect = () => {
    console.log("button was clicked");
    props.history.push("/dashboard/heartbeat/feed");
  };

  return (
    <div>
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>
          {gateways.map(gateway => {
            return (
              <Grid item xs={3}>
                <Card
                  title="Gateway ID"
                  gwID={gateway.gwID}
                  manager={gateway.manager}
                  status={gateway.active}
                  name={gateway.name}
                  turbine={gateway.turbines.length}
                  cb={handleRedirect}
                />
              </Grid>
            );
          })}
        </Grid>
      </Container>
    </div>
  );
};

export default withRouter(Heartbeat);
