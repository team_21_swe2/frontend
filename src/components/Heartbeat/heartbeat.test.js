import { cleanup } from '@testing-library/react'
import "@testing-library/jest-dom/extend-expect";
import axiosMock from "axios";

afterEach(cleanup);

it("Fetches heartbeat data", async () => {
  axiosMock.get.mockResolvedValueOnce({
    heartbeats: [
      {
        _id: 1,
        sentTime: Date,
        receivedTime: Date,
      },
      {
        _id: 2,
        sentTime: Date,
        receivedTime: Date,
      },
      {
        _id: 3,
        sentTime: Date,
        receivedTime: Date,
      }
    ]
  });
});