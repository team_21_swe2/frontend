import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { green, pink } from "@material-ui/core/colors";

const useStyles = makeStyles({
  card: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

const MediaCard = props => {
  const classes = useStyles();

  const activeStatus = status => {
    if (status || props.gwID === 342984) {
      return { color: green[500] };
    } else {
      return { color: pink[500] };
    }
  };

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {props.title}
        </Typography>
        <Typography component="p" variant="h3">
          {props.gwID}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Manager:<b> {props.manager}</b> <br />
          GW name: <b> {props.name}</b> <br />
          Turbine count: <b> {props.turbine}</b> <br />
          Status: <FiberManualRecordIcon style={activeStatus(props.status)} />
        </Typography>
      </CardContent>
      <CardActions>
        {props.gwID === 342984 ? (
          <Button size="small" color="primary" onClick={() => props.cb(this)}>
            Heartbeat Feed
          </Button>
        ) : (
          <Button
            size="small"
            color="primary"
            onClick={() => props.cb(this)}
            disabled
          >
            Heartbeat Feed
          </Button>
        )}
      </CardActions>
    </Card>
  );
};

export default MediaCard;
