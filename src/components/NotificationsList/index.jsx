import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from "@material-ui/core/";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));


export default function SimpleTable(props) {
  console.log(props.notification)
  const [notificationState, setNotificationState] = React.useState(props.notification);
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell><b>Error</b></TableCell>
            <TableCell><b>Delete Action</b></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {notificationState.map((el, i) => {
            return (
              <TableRow key={`row-${i}`}>
                <TableCell>{el.content}</TableCell>
                <TableCell>
                  <Button
                    onClick={props.del.bind(this, i)}
                    color="secondary"
                  >
                    Delete
                    </Button>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}



  // const useStyles = makeStyles(theme => ({
//   root: {
//     width: "100%",
//     marginTop: theme.spacing.unit * 3,
//     overflowX: "auto"
//   },
//   table: {}
// }));
