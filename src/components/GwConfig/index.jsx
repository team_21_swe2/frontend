import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import RouterIcon from '@material-ui/icons/Router';
import SystemUpdateAltIcon from '@material-ui/icons/SystemUpdateAlt';
import ToysIcon from '@material-ui/icons/Toys';
import SpeedIcon from '@material-ui/icons/Speed';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AllInboxIcon from '@material-ui/icons/AllInbox';
import AddGateway from "./addGW"
import AddTurbine from "./addTurbine"
import SoftwareList from "./softwareList"
import TrubineToGW from "./turbineToGW"
import HBFreq from "./hbFreq"
import TestGW from "./testGW"
import AddSW from "./addSW"
import "./index.css"

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonForce() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default" className="button">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
          centered
        >
          <Tab label="Add Gateway" icon={<RouterIcon />} {...a11yProps(0)} />
          <Tab label="Add Turbine" icon={<ToysIcon />} {...a11yProps(1)} />
          <Tab label="Turbine to GW" icon={<AccountTreeIcon />} {...a11yProps(2)} />
          <Tab label="Mod HB Freq" icon={<SpeedIcon />} {...a11yProps(3)} />
          <Tab label="GW Diag Test" icon={<AssessmentIcon />} {...a11yProps(4)} />
          <Tab label="Add GW SW" icon={<SystemUpdateAltIcon />} {...a11yProps(5)} />
          <Tab label="GW SW Packages" icon={<AllInboxIcon />} {...a11yProps(6)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <AddGateway />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <AddTurbine />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <TrubineToGW />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <HBFreq />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <TestGW />
      </TabPanel>
      <TabPanel value={value} index={5}>
        <AddSW />
      </TabPanel>
      <TabPanel value={value} index={6}>
        <SoftwareList />
      </TabPanel>
    </div >
  );
}
