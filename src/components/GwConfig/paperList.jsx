import React from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { makeStyles, Container, Grid } from "@material-ui/core";
import LinkIcon from "@material-ui/icons/Link";
import DescriptionIcon from "@material-ui/icons/Description";
import "./index.css";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    container: {
      marginLeft: 57,
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4)
    }
  }
}));

export default function PaperList(props) {
  const classes = useStyles();

  return (
    <div className="paper-space">
      <Container maxWidth="lg" className={classes.container}>
        <Paper className={classes.root}>
          <Typography variant="h5" component="h3">
            {props.title}
          </Typography>
          <Typography component="p">
            <LinkIcon color="primary" /> <a href={props.link}>{props.link} </a>
            <br />
            <DescriptionIcon color="primary" /> {props.description}
          </Typography>
        </Paper>
      </Container>
    </div>
  );
}
