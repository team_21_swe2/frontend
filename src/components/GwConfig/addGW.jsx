import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from "@material-ui/icons/Save";
import axios from "axios"

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    maxWidth: 750,
    // display: "box",
    margin: "auto"
  },
  button: {
    margin: theme.spacing(1)
  }
}));

export default function PaperSheet() {
  const [addGateway, setAddGateways] = React.useState("");
  const [addManager, setAddManager] = React.useState("");
  const classes = useStyles();


  const onSubmit = () => {
    console.log(addGateway);
    console.log(addManager);

    axios
      .post(
        `https://team21.softwareengineeringii.com/api/gateways/add`, {
          name: addGateway,
          manager: addManager
        }).then(res => {
          console.log("submitted")
        });
    setAddGateways("")
    setAddManager("")
    alert("Add Gateway has been submitted")
  };

  return (
    <Paper className={classes.root}>
      <Typography variant="h4" component="h3">
        Add a gateway
      </Typography>
      <Typography color="textSecondary" component="p">
        GW status will be listed as inactive, until live stream detected by Command in Control (CiC)

      </Typography>
      <br />
      <TextField
        id="filled-full-width"
        label="Enter a gateway name"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setAddGateways(e.target.value)}
        value={addGateway}
      />
      <br />
      <Typography component="p">
        <br />
      </Typography>
      <TextField
        id="filled-full-width"
        label="Assign to a manager"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setAddManager(e.target.value)}
        value={addManager}
      />
      <br />
      <br />
      <Button
        variant="contained"
        color="primary"
        size="secondary"
        className={classes.button}
        startIcon={<SaveIcon />}
        onClick={onSubmit}
      >
        Save
      </Button>
    </Paper>
  );
}