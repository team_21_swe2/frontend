import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from "@material-ui/icons/Save";
import axios from "axios"

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    maxWidth: 750,
    // display: "box",
    margin: "auto"
  },
  button: {
    margin: theme.spacing(1)
  }
}));

export default function PaperSheet() {
  const [addLat, setLat] = React.useState(number);
  const [addLong, setLong] = React.useState(number);
  const classes = useStyles();


  const onSubmit = () => {
    console.log(addLat);
    console.log(addLong);

    axios
      .post(
        `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/turbines/add`, {
          lat: addLat,
          long: addLong
        }).then(res => {
          console.log("submitted")
        });
    setLat("")
    setLong("")
    alert("Add turbine has been submitted")
  };

  return (
    <Paper className={classes.root}>
      <Typography variant="h4" component="h3">
        Add a turbine
      </Typography>
      <Typography color="textSecondary" component="p">
        Turbine will need to be assigned to GW following submission

      </Typography>
      <br />
      <TextField
        id="filled-full-width"
        label="Enter turbine latitude"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setLat(e.target.value)}
        value={addLat}
      />
      <br />
      <Typography component="p">
        <br />
      </Typography>
      <TextField
        id="filled-full-width"
        label="Enter turbine longitude"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setLong(e.target.value)}
        value={addLong}
      />
      <br />
      <br />
      <Button
        variant="contained"
        color="primary"
        size="secondary"
        className={classes.button}
        startIcon={<SaveIcon />}
        onClick={onSubmit}
      >
        Save
      </Button>
    </Paper>
  );
}