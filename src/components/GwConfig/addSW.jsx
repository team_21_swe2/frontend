import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from "@material-ui/icons/Save";
import MenuItem from '@material-ui/core/MenuItem';
import axios from "axios"
import CircularProgress from '@material-ui/core/CircularProgress';
import "./index.css"


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    maxWidth: 750,
    // display: "box",
    margin: "auto"
  },
  button: {
    margin: theme.spacing(1)
  }
}));

export default function PaperSheet() {
  const [title, setTitle] = React.useState("");
  const [link, setLink] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [loaded, setLoaded] = React.useState(false);

  const classes = useStyles();

  const onSubmit = () => {
    console.log(title)
    console.log(link)
    console.log(description)
  
    axios
      .post(
        `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/softwareLinks/add`, {
          title: title,
          Link: link,
          Description: description
        }).then(res => {
          console.log("submitted")
        });
    setTitle("")
    setLink("")
    setDescription("")
    alert("Add Software package has been submitted")
  };

  return (
    <Paper className={classes.root}>
      <Typography variant="h4" component="h3">
        Add a software package
      </Typography>
      <Typography color="textSecondary" component="p">
        Gateway software package updates are not automated i.e. require tech installation
      </Typography>
      <br />
      <TextField
        id="filled-full-width"
        label="Enter a title"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setTitle(e.target.value)}
        SelectProps={{
          MenuProps: {
            className: classes.menu,
          },
        }}
        value={title}
      />
      <br />
      <Typography component="p">
        <br />
      </Typography>
      <TextField

        id="filled-full-width"
        label="Enter the package download link"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setLink(e.target.value)}
        value={link}
      />
      <Typography component="p">
        <br />
      </Typography>
      <TextField
        id="filled-full-width"
        label="Provide a brief description of the software functionality"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setDescription(e.target.value)}
        value={description}
      />
      <br />
      <br />
      <Button
        variant="contained"
        color="primary"
        size="secondary"
        className={classes.button}
        startIcon={<SaveIcon />}
        onClick={onSubmit}
      >
        Save
      </Button>
    </Paper>
  )
}
