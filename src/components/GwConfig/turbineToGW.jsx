import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from "@material-ui/icons/Save";
import MenuItem from '@material-ui/core/MenuItem';
import axios from "axios"
import CircularProgress from '@material-ui/core/CircularProgress';
import "./index.css"


const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    maxWidth: 750,
    // display: "box",
    margin: "auto"
  },
  button: {
    margin: theme.spacing(1)
  }
}));

export default function PaperSheet() {
  const [gateway, setGateway] = React.useState("");
  const [gatewayList, setGatewayList] = React.useState([])
  const [turbineList, setTurbineList] = React.useState([])
  const [turbine, setTurbine] = React.useState("");
  const [loaded, setLoaded] = React.useState(false);
  React.useEffect(() => {
    fetchData();
  }, []);
  const classes = useStyles();

  const fetchData = async () => {
    let fetchGatewaysResp = await fetch(
      `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/gateways`,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    if (fetchGatewaysResp.ok) {
      const gatewayData = await fetchGatewaysResp.json();
      for (let i = 0; i < gatewayData.length; i++) {
        var gatewayObj = {};
        gatewayObj.label = gatewayData[i].name;
        gatewayObj.value = JSON.stringify(gatewayData[i].gwID);
        gatewayList.push(await gatewayObj);
      }
      // setGatewayList(gatewayList)
      console.log(gatewayList)
    }
    let fetchTurbineResp = await fetch(
      `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/turbines/`,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    if (fetchTurbineResp.ok) {
      const turbineData = await fetchTurbineResp.json();
      for (let i = 0; i < turbineData.length; i++) {
        var turbineObj = {};
        turbineObj.value = JSON.stringify(turbineData[i].wtID);
        turbineList.push(await turbineObj);
      }
      // setGatewayList(gatewayList)
      console.log(gatewayList)
    }
    setLoaded(true);
  };

  const onSubmit = () => {
    console.log(gateway)
    console.log(turbine)
    axios
      .post(
        `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/gateways/updateTurbineList`, {
          turbines: [turbine],
          gwID: gateway
        }).then(res => {
          console.log("submitted")
        });
    setGateway("")
    setTurbine("")
    alert("Turbine has been added to gateway")
  };

  return loaded && gatewayList ? (
    <Paper className={classes.root}>
      <Typography variant="h4" component="h3">
        Assign new turbine to gateway
      </Typography>
      <Typography color="textSecondary" component="p">
        Pending gateway status, turbine data will feed to gateway

      </Typography>
      <br />
      <TextField
        select
        id="filled-full-width"
        label="Pick a gateway"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setGateway(e.target.value)}
        SelectProps={{
          MenuProps: {
            className: classes.menu,
          },
        }}
        value={gateway}
      >
        {gatewayList.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
      <br />
      <Typography component="p">
        <br />
      </Typography>
      <TextField
        select
        id="filled-full-width"
        label="Assign a turbine"
        style={{ margin: 8 }}
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="filled"
        onChange={e => setTurbine(e.target.value)}
        value={turbine}
      >
        {turbineList.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.value}
          </MenuItem>
        ))}
      </TextField>
      <br />
      <br />
      <Button
        variant="contained"
        color="primary"
        size="secondary"
        className={classes.button}
        startIcon={<SaveIcon />}
        onClick={onSubmit}
      >
        Save
      </Button>
    </Paper>
  ) : (
      <div className="loader">
        <CircularProgress />
      </div>
    );
}
