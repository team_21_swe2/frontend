import React from 'react';
import { makeStyles, Container } from "@material-ui/core";
import PaperList from "./paperList"
import "./index.css"

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    container: {
      marginLeft: 57,
      paddingTop: theme.spacing(4) + "25px",
      paddingBottom: theme.spacing(4)
    }
  },
}));


export default function SwList() {
  const classes = useStyles();
  const [softwares, setSoftwares] = React.useState([]);
  React.useEffect(() => {
    fetchSoftwares();
  }, []);

  const fetchSoftwares = async () => {
    let fetchSoftwaresResp = await fetch(
      `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/softwareLinks/`,
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    if (fetchSoftwaresResp.ok) {
      setSoftwares(await fetchSoftwaresResp.json());
    }
  };

  const onHeartbeatChange = async () => {
    fetchSoftwares();
  };

  return (
    <div className="paper-space">
      <Container maxWidth="lg" className={classes.container}>
        {softwares.map(software => {
          return (
            <PaperList
              title={software.title}
              link={software.link}
              description={software.description}
            />
          );
        })}
      </Container>
    </div>
  );
}