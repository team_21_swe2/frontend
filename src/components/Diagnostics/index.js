import React, { Component } from "react";
import MaterialTable from "material-table";
import axios from "axios";
import "./index.css";

export default class Diagnostics extends Component {
  constructor(props) {
    super(props);

    this.state = {
      diagnostics: []
    };
  }
  // CORS conflict from localhost...
  // Remove base URL (https://team21.softwareengineeringii.com) on server deployment
  // TODO: Add env variables to work for in-dev and proc, without having to change the URL
  componentDidMount() {
    axios
      .get(
        `https://cors-anywhere.herokuapp.com/https://team21.softwareengineeringii.com/api/diagnostics`
      )
      .then(res => {
        const diagnostics = res.data;
        console.log(diagnostics);
        this.setState({ diagnostics });
      });
  }

  render() {
    // Displays a list of all diagnostics returned from server
    var { diagnostics } = this.state;
    return (
      <div style={{ maxWidth: "100%" }}>
        <MaterialTable
          options={{ pageSize: 10 }}
          columns={[
            { title: "GW ID", field: "gwID", type: "numeric" },
            { title: "Gearbox Temp", field: "gearboxTemp", type: "numeric" },
            { title: "Rotor Temp", field: "rotorTemp", type: "numeric" },
            { title: "Rotor Speed", field: "rotorSpeed", type: "numeric" },
            { title: "Grid Voltage", field: "gridVoltage", type: "numeric" },
            { title: "Wind Speed", field: "windSpeed", type: "numeric" },
            { title: "Date", field: "receivedTime.date", type: "date" },
            { title: "Time", field: "receivedTime.time", type: "date" }
          ]}
          data={diagnostics}
          title="Diagnostic Results"
        />
      </div>
    );
  }
}
