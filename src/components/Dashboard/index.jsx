import React from "react";
import { withRouter, Switch } from "react-router-dom";
import Heartbeat from "../Heartbeat";
import Home from "../Home";
import Navigation from "../Navigation";
import Viz from "../../containers/Tableau";
import Diagnostics from "../Diagnostics";
import TestSuit from "../TestSuit";
import HeartbeatFeed from "../Heartbeat/heartbeatFeed";
import SwList from "../SwList";
import GwConfig from "../GwConfig"
import NotificationsList from "../NotificationsList"
import { ProtectedRoute } from "../../protected-route";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import 'react-notifications/lib/notifications.css';
import "./index.css";
const URL = "wss://team21.softwareengineeringii.com:3030";


// Nested router, so path is passed through "useRouteMatch"
class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0,
      notificationList: []
    };
    this.deleteItem = this.deleteItem.bind(this)
    this.displayNotification = this.displayNotification.bind(this)
  }

  deleteItem(i) {
    const { notificationList } = this.state;
    notificationList.splice(i, 1);
    this.setState({ notificationList });
    this.setState({ count: this.state.count - 1 })
  }

  displayNotification = notification => {

    console.log("[test1] " + notification.type + "//" + notification.content);
    this.setState({ count: this.state.count + 1 })
    this.setState({
      notificationList: this.state.notificationList.concat({
        type: notification.type,
        content: notification.content
      })
    })
    console.log(this.state.notificationList)
    switch (notification.type) {
      case "warning":

        NotificationManager.warning(notification.content, "ATTENTION!", 3000);
        break;
      case "error":

        NotificationManager.error(
          notification.content,
          "WARNING!",
          3000,
          () => {
            alert("callback");
          }
        );
        break;
    }
  };

  ws = new WebSocket(URL);

  componentDidMount() {
    this.ws.onopen = () => {
      // on connecting, do nothing but log it to the console
      console.log("connected");
    };

    this.ws.onmessage = evt => {
      const notification = evt.data;
      console.log("Received notification: " + JSON.parse(notification));
      this.displayNotification(JSON.parse(notification));
    };

    this.ws.onclose = () => {
      console.log("disconnected");
      // automatically try to reconnect on connection loss
      ws: new WebSocket(URL);
    };
  }

  render() {
    const { path, url } = this.props.match;
    return (
      <div className="dashboard-body">
        <NotificationContainer />
        <Navigation notificationCount={this.state.count} />
        <Switch>
          <ProtectedRoute exact path={`${path}/home`}>
            <Home />
          </ProtectedRoute>
          <ProtectedRoute exact path={`${path}/heartbeat`}>
            <Heartbeat />
          </ProtectedRoute>
          <ProtectedRoute path={`${path}/viz/daily`}>
            <Viz vizReq="daily" />
          </ProtectedRoute>
          <ProtectedRoute path={`${path}/viz/on-demand`}>
            <Viz vizReq="on-demand" />
          </ProtectedRoute>
          <ProtectedRoute path={`${path}/diagnostics`}>
            <Diagnostics />
          </ProtectedRoute>
          <ProtectedRoute path={`${path}/heartbeat/feed`}>
            <HeartbeatFeed />
          </ProtectedRoute>
          <ProtectedRoute path={`${path}/admin-suit`}>
            <GwConfig />
          </ProtectedRoute>
          <ProtectedRoute path={`${path}/notifications`}>
            <NotificationsList notification={this.state.notificationList} del={this.deleteItem} />
          </ProtectedRoute>
        </Switch>
      </div>
    );
  }
}

export default withRouter(Dashboard)


