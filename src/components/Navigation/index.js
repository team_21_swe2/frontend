import React from "react";
import { Link, useRouteMatch, withRouter } from "react-router-dom";
import { Nav, Navbar, NavDropdown } from "react-bootstrap";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Badge from "@material-ui/core/Badge";
import { IconButton } from "@material-ui/core";
import { grey } from "@material-ui/core/colors";
import "./index.css";
import "bootstrap";
import auth from "../../auth";

const Navigation = props => {
  let { url } = useRouteMatch();

  const handleLogOut = () => {
    auth.logout();
    props.history.push("/login");
  };

  return (
    <Navbar bg="dark" variant="dark" expand="lg" className="navbar">
      <Navbar.Brand>
        <img
          alt=""
          src="/images/Cielo.png"
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{" "}
        <b>Cielo Wind Farm</b>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={Link} to={`${url}/home`}>
            Home
          </Nav.Link>
          <Nav.Link as={Link} to={`${url}/heartbeat`}>
            Heartbeat
          </Nav.Link>
          <Nav.Link as={Link} to={`${url}/diagnostics`}>
            Diagnostics
          </Nav.Link>
          <NavDropdown title="Data-Viz" id="basic-nav-dropdown">
            <NavDropdown.Item as={Link} to={`${url}/viz/daily`}>
              Daily
            </NavDropdown.Item>
            <NavDropdown.Item as={Link} to={`${url}/viz/on-demand`}>
              On-Demand
            </NavDropdown.Item>
          </NavDropdown>
          <div className="divider-vertical" />
          <Nav.Link as={Link} to={`${url}/admin-suit`}>
            Admin Suite
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
      <Navbar.Collapse className="justify-content-end">
        <Nav.Link as={Link} to={`${url}/notifications`}>
          <IconButton style={{ color: grey[50] }}>
            <Badge badgeContent={props.notificationCount} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Nav.Link>
        <IconButton onClick={handleLogOut} style={{ color: grey[50] }}>
          <ExitToAppIcon />
        </IconButton>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default withRouter(Navigation);
