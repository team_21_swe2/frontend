import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { Button, FormGroup, FormControl, FormLabel, Alert } from "react-bootstrap";
import Dashboard from "../Dashboard";
import axios from "axios";
import auth from "../../auth";
import './index.css';


export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      isAuthenticated: false,
      loginError: false
    };
  }

  // User alert to of blank email or password field
  validateForm() {
    return this.state.username.length > 0 && this.state.password.length > 0;
  }


  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  // No authentication at this time. Endpoint need to be created 
  handleSubmit = async event => {
    event.preventDefault();
    // console.log("submitted");
    axios
      .post("/api/users/login",
        {
          username: this.state.username,
          password: this.state.password
        })
      .then(res => {
        console.log(res)
        if (res.status === 200) {
          auth.login();
          this.setState({ isAuthenticated: true });
          console.log(this.state.isAuthenticated)
        }
      }).catch(err => {
        // handle error
        this.setState({ loginError: true });
      })
  };

  render() {
    // Condition to check user athentication
    if (this.state.isAuthenticated) {
      return <Redirect to="/dashboard/home" component={Dashboard} />
    } else {
      return (
        <div id="login" className="Login">
          <div className="form-border">
            <h1> Sign In </h1>
            <form onSubmit={this.handleSubmit}>
              <FormGroup controlId="username" bsSize="large">
                <FormLabel></FormLabel>
                <FormControl
                  autoFocus
                  type="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                  placeholder="Username"
                />
              </FormGroup>
              <FormGroup controlId="password" bsSize="large">
                {/* <FormLabel></FormLabel> */}
                <FormControl
                  value={this.state.password}
                  onChange={this.handleChange}
                  type="password"
                  placeholder="Password"
                />
              </FormGroup>
              <Button
                block
                bsSize="large"
                disabled={!this.validateForm()}
                type="submit"
              >
                Login
          </Button>
            </form>
            <br />
            <div>
              {this.state.loginError &&
                <Alert variant="danger">
                  There were problems with signing in. Try again.{' '}
                </Alert>
              }
            </div>
          </div>
        </div>
      );
    }
  }
}

