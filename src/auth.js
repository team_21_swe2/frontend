//https://www.youtube.com/watch?v=Y0-qdp-XBJg
//https://stackoverflow.com/questions/49788580/how-to-redirect-to-correct-client-route-after-social-auth-with-passport-react
class Auth {
  constructor() {
    this.authenticated = false;
  }

  login() {
    this.authenticated = true;
  }

  logout() {
    this.authenticated = false;
  }
  isAuthenticated() {
    return this.authenticated;
  }
}

export default new Auth();
