Author: Simon Gohl

SWEii Team21: Cielo Wind Farms

# We-IoT Web-app

We-IoT takes real-time data and converts that into actionable insights. Using industry specific KPI's, business decisions are now driven by data analytics. This improves the overall business missions.

## Ceilo Wind Farm

This particular web-app is geared to Cielo Wind Farm solutions. The We-IoT solution is predictive maintenance. This is done by getting a real-time data from IoT devices and generating a data viz to identify low/high KPI's in a glance. As result, maintenance cost is lowered, turbine energy throughout is put, down-time down, and overall time saved.

## Use-case

End-users will use a login portal to access the We-IoT web-app. From there a user may pull _Heartbeat_ data to show the IoT device is live, analysis data viz graphs, get health on IoT devices, and many other features.

## Technology

The webapp technology stack uses the Node.js with the React library. The stack also utilizes dependencies like Axios and Material-UI.

These technologies allow for the web-app

## Comments, etc.

The codebase is build for maintainability with clean code. Comments details the what's and why's.

`TODO` and `TECH-DEBT` indicate features to be build out.

## Run app

To run the app (under existing backend functionality), pull from repo and install all dependencies and run the command: `npm start`
